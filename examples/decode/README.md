# 使用 MAZ-G.726 解码 G.726 音频

输入 G.726 音频文件，输出 PCM 音频文件。

## 编译

```
paul@vmware:~/gitee/maz-g726/demo/decode$ make
gcc -I./ -I../../  -c decode.c -o decode.o
gcc -I./ -I../../  -c ../../maz_cpnt_g726.c -o ../../maz_cpnt_g726.o
gcc decode.o ../../maz_cpnt_g726.o -I./ -I../../  -o decode
paul@vmware:~/gitee/maz-g726/demo/decode$ 
```

## 解码

```
./decode 5 5.g726 5.pcm
./decode 4 4.g726 4.pcm
./decode 3 3.g726 3.pcm
./decode 2 2.g726 2.pcm
```

说明，平台的字节序是小端序，因此最后输出的 PCM 音频是 s16le 格式。

使用 ffplay 可以播放 pcm 音频文件

```
ffplay -ar 8000 -ac 1 -f s16le -i 2.pcm
ffplay -ar 8000 -ac 1 -f s16le -i 3.pcm
ffplay -ar 8000 -ac 1 -f s16le -i 4.pcm
ffplay -ar 8000 -ac 1 -f s16le -i 5.pcm
```

